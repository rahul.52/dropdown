﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DropDown.Models;

namespace DropDown.Controllers
{
    public class DdownController : Controller
    {


        private test GetDD()
        {
            if (Session["testAccount"] == null)
            {
                Session["testAccount"] = new test();
            }
            return (test)Session["testAccount"];
        }

        private void RemoveDD()
        {
            Session.Remove("testAccount");
        }

        public ActionResult Index()
        {
            List<Province> provinceList = new List<Province>();
            List<District> districtList = new List<District>();
            List<City> cityList = new List<City>();

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                provinceList = db.Query<Province>("Select * From Province").ToList();
            }
            ViewBag.province = new SelectList(provinceList, "ProvinceId", "ProvinceName");
            return View();
        }

        public JsonResult GetDistrictList(int ProvinceId)
        {
            List<District> districtList = new List<District>();
            
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                districtList = db.Query<District>("Select * From District where ProvinceId = @ProvinceId", new { ProvinceId }).ToList();
                
            }
            return Json(districtList, JsonRequestBehavior.AllowGet);
        }
    }
}