﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DropDown.Models
{
    public class District
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }

        public Province Province { get; set; }
        public int ProvinceId { get; set; }
    }
}