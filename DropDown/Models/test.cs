﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DropDown.Models
{
    public class test
    {
        public int CId { get; set; }
        public string CityName { get; set; }
        public int DId { get; set; }
        public string DistrictName { get; set; }
        public int PId { get; set; }
        public string ProvinceName { get; set; }
    }
}