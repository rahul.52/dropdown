﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DropDown.Models
{
    public class City
    {
        public int CId { get; set; }
        public string CityName { get; set; }

        public District District { get; set; }
        public int DId { get; set; }
    }
}