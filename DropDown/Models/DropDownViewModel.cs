﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DropDown.Models
{
    public class DropDownViewModel
    {
        public Province Province { get; set; }
        [Display(Name = "Province")]
        public int ProvinceId { get; set; }
        

        public District District { get; set; }
        [Display(Name = "District")]
        public int DistrictId { get; set; }

        public City City { get; set; }
        [Display(Name = "City")]
        public int CId { get; set; }
    }
}