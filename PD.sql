USE [test]
GO
/****** Object:  Table [dbo].[District]    Script Date: 11/03/19 4:08:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[DistrictId] [int] NULL,
	[DistrictName] [nchar](50) NULL,
	[ProvinceId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Province]    Script Date: 11/03/19 4:08:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Province](
	[ProvinceId] [int] NULL,
	[ProvinceName] [nchar](50) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (1, N'Bhojpur                                           ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (2, N'Dhankuta                                          ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (3, N'Ilam                                              ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (4, N'Jhapa                                             ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (5, N'Khotang                                           ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (6, N'Morang                                            ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (7, N'Okhaldhunga                                       ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (8, N'Panchthar                                         ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (9, N'Sankhuwasabha                                     ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (10, N'Solukhumbu                                        ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (11, N'Sunsari                                           ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (12, N'Taplejung                                         ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (13, N'Terhathum                                         ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (14, N'Udayapur                                          ', 1)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (15, N'Saptari                                           ', 2)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (16, N'Siraha                                            ', 2)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (17, N'Dhanusa                                           ', 2)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (18, N'Mahottari                                         ', 2)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (19, N'Sarlahi                                           ', 2)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (20, N'Bara                                              ', 2)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (21, N'Parsa                                             ', 2)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (22, N'Rautahat                                          ', 2)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (23, N'Sindhuli                                          ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (24, N'Ramechhap                                         ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (25, N'Dolakha                                           ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (26, N'Bhaktapur                                         ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (27, N'Dhading                                           ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (28, N'Kathmandu                                         ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (29, N'Kavrepalanchok                                    ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (30, N'Lalitpur                                          ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (31, N'Nuwakot                                           ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (32, N'Rasuwa                                            ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (33, N'Sindhupalchok                                     ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (34, N'Chitwan                                           ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (35, N'Makwanpur                                         ', 3)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (36, N'Baglung                                           ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (37, N'Gorkha                                            ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (38, N'Kaski                                             ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (39, N'Lamjung                                           ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (40, N'Manang                                            ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (41, N'Mustang                                           ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (42, N'Myagdi                                            ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (43, N'Nawalpur                                          ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (44, N'Parbat                                            ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (45, N'Syangja                                           ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (46, N'Tanahun                                           ', 4)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (47, N'Kapilvastu                                        ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (48, N'Parasi                                            ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (49, N'Rupandehi                                         ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (50, N'Arghakhanchi                                      ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (51, N'Gulmi                                             ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (52, N'Palpa                                             ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (53, N'Dang                                              ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (54, N'Pyuthan                                           ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (55, N'Rolpa                                             ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (56, N'Eastern                                           ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (57, N'Banke                                             ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (58, N'Bardiya                                           ', 5)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (59, N'Western Rukum                                     ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (60, N'Salyan                                            ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (61, N'Dolpa                                             ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (62, N'Humla                                             ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (63, N'Jumla                                             ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (64, N'Kalikot                                           ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (65, N'Mugu                                              ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (66, N'Surkhet                                           ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (67, N'Dailekh                                           ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (68, N'Jajarkot                                          ', 6)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (69, N'Kailali                                           ', 7)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (70, N'Achham                                            ', 7)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (71, N'Doti                                              ', 7)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (72, N'Bajhang                                           ', 7)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (73, N'Bajura                                            ', 7)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (74, N'Kanchanpur                                        ', 7)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (75, N'Dadeldhura                                        ', 7)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (76, N'Baitadi                                           ', 7)
INSERT [dbo].[District] ([DistrictId], [DistrictName], [ProvinceId]) VALUES (77, N'Darchula                                          ', 7)
INSERT [dbo].[Province] ([ProvinceId], [ProvinceName]) VALUES (1, N'Province No 1                                     ')
INSERT [dbo].[Province] ([ProvinceId], [ProvinceName]) VALUES (2, N'Province No 2                                     ')
INSERT [dbo].[Province] ([ProvinceId], [ProvinceName]) VALUES (3, N'Province No 3                                     ')
INSERT [dbo].[Province] ([ProvinceId], [ProvinceName]) VALUES (4, N'Province No 4                                     ')
INSERT [dbo].[Province] ([ProvinceId], [ProvinceName]) VALUES (5, N'Province No 5                                     ')
INSERT [dbo].[Province] ([ProvinceId], [ProvinceName]) VALUES (6, N'Province No 6                                     ')
INSERT [dbo].[Province] ([ProvinceId], [ProvinceName]) VALUES (7, N'Province No 7                                     ')
